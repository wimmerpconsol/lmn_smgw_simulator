#include "smgw/sml.h"
#include "smgw/cosem.h"
#include "smgw/error.h"
#include "smgw/log.h"
#include "smgw/obis.h"
#include <smlpp/file.h>

using namespace ::sml;

namespace smgw::sml
{

octet_string object_class_id(const std::array<std::uint8_t, 6>& obis_number, cosem::id cosem_id)
{
	auto object_class_id = octet_string(9);
	auto it = std::copy(obis_number.begin(), obis_number.end(), object_class_id.begin());
	*it++   = enum_val(cosem_id) >> 8;
	*it++   = enum_val(cosem_id) & 0xff;
	*it++   = 0x00; // version is pretty much always zero

	return object_class_id;
}

tree_path parameter_tree_path(const std::array<std::uint8_t, 6>& obis_number, cosem::id cosem_id)
{
	auto path_entries = sequence<octet_string>{};
	path_entries.emplace_back(object_class_id(obis_number, cosem_id));

	for (unsigned8 index = 1; index <= cosem::num_attributes(cosem_id); ++index) {
		path_entries.push_back({0x00, index});
	}

	return tree_path{std::move(path_entries)};
}

template <typename T>
T get_proc_par_value(const proc_par_value& p)
{
	// TODO: throw custom exceptions?
	const auto& value_v = std::get<value>(p);

	if constexpr (std::is_integral_v<T>) {
		return std::visit(overloaded{

		                      [](integer8 i) { return static_cast<T>(i); },
		                      [](integer16 i) { return static_cast<T>(i); },
		                      [](integer32 i) { return static_cast<T>(i); },
		                      [](integer64 i) { return static_cast<T>(i); },
		                      [](unsigned8 i) { return static_cast<T>(i); },
		                      [](unsigned16 i) { return static_cast<T>(i); },
		                      [](unsigned32 i) { return static_cast<T>(i); },
		                      [](unsigned64 i) { return static_cast<T>(i); },

		                      [](auto) -> T { throw std::bad_variant_access(); }},
		                  value_v);
	}
	else {
		return std::get<T>(value_v);
	}
}

// specialization
template <>
cosem::scal_unit_type get_proc_par_value(const proc_par_value& p)
{
	// TODO: throw custom exceptions?
	const auto& value_v                  = std::get<value>(p);
	const auto& list_type_v              = std::get<list_type>(value_v);
	const auto& cosem_value_v            = std::get<cosem_value>(list_type_v);
	const auto& cosem_scaler_unit_type_v = std::get<cosem_scaler_unit_type>(cosem_value_v);

	return cosem::scal_unit_type{cosem_scaler_unit_type_v.scaler,
	                             static_cast<cosem::unit_type>(cosem_scaler_unit_type_v.unit)};
}

struct message_body_visitor
{
	template <typename MessageBody>
	std::optional<cosem::object> operator()(MessageBody const&) const
	{
		return {};
	}

	std::optional<cosem::object> operator()(get_proc_parameter_response const& response) const
	{
		auto log = spdlog::get("sml");

		const auto& parameter_tree   = response.parameter_tree;
		const auto& child_list       = parameter_tree.child_list;
		const auto& obis_cosem       = parameter_tree.parameter_name;
		const auto& cosem_attributes = child_list.value();

		auto const cosem_id  = static_cast<cosem::id>((obis_cosem[6] << 8) | obis_cosem[7]);
		const auto obis_code = [&]() {
			std::uint64_t val = 0;
			for (auto it = obis_cosem.begin(); it < obis_cosem.begin() + 6; ++it) {
				val = (val << 8) | *it;
			}
			return static_cast<obis::code>(val);
		}();

		if (!child_list.has_value() || child_list->size() != cosem::num_attributes(cosem_id)) {
			throw std::system_error(sml_errc::object_attributes_missing);
		}

		const auto& response_logical_name =
		    get_proc_par_value<cosem::octet_string>(*cosem_attributes[0].parameter_value);

		switch (cosem_id) {
		case cosem::id::data: {
			auto response_value = [&]() -> decltype(cosem::data::value) {
				switch (obis_code) {
				case obis::code::time:
				case obis::code::status: {
					auto val =
					    get_proc_par_value<cosem::unsigned32>(*cosem_attributes[1].parameter_value);
					log->debug("Response value: {}", val);
					return val;
				}
				case obis::code::maximum_fragment_size: {
					auto val =
					    get_proc_par_value<cosem::unsigned16>(*cosem_attributes[1].parameter_value);
					log->debug("Response value: {}", val);
					return val;
				}
				case obis::code::device_class:
				case obis::code::manufacturer_id:
				case obis::code::device_id:
				case obis::code::public_key:
				case obis::code::device_fw_version:
				case obis::code::fw_checksum: {
					auto val = get_proc_par_value<cosem::octet_string>(
					    *cosem_attributes[1].parameter_value);
					log->debug("Response value:\n{}", log_helper{val});
					return val;
				}

				default: throw std::system_error(sml_errc::unsupported_obis_number);
				}
			}();
			return cosem::data{response_logical_name, response_value};
		} break;

		case cosem::id::registr: {
			auto response_value = [&]() -> decltype(cosem::registr::value) {
				switch (obis_code) {
				case obis::code::voltage_l1_instantaneous:
				case obis::code::voltage_l2_instantaneous:
				case obis::code::voltage_l3_instantaneous: {
					return get_proc_par_value<cosem::unsigned64>(
					    *cosem_attributes[1].parameter_value);
				}
				case obis::code::active_power_instantaneous: {
					return get_proc_par_value<cosem::signed64>(
					    *cosem_attributes[1].parameter_value);
				}
				default: throw std::system_error(sml_errc::unsupported_obis_number);
				}
			}();
			return cosem::registr{
			    response_logical_name, response_value,
			    get_proc_par_value<cosem::scal_unit_type>(*cosem_attributes[2].parameter_value)};
		} break;

		case cosem::id::advanced_extended_register: {
			auto object = cosem::advanced_extended_register{
			    response_logical_name,
			    get_proc_par_value<cosem::unsigned64>(*cosem_attributes[1].parameter_value),
			    get_proc_par_value<cosem::scal_unit_type>(*cosem_attributes[2].parameter_value),
			    get_proc_par_value<cosem::unsigned32>(*cosem_attributes[3].parameter_value),
			    get_proc_par_value<cosem::unsigned32>(*cosem_attributes[4].parameter_value)};
			log->debug("Response value: {} k{}",
			           object.value * std::pow(10, object.scaler_unit.scaler) / 1000,
			           cosem::to_string(object.scaler_unit.unit));
			return object;
		} break;

		default: throw std::system_error(sml_errc::unsupported_cosem_class);
		}

		return {};
	}

	std::optional<cosem::object> operator()(attention_response const& a) const
	{
		int attention_code = (*(a.attention_no.end() - 2) << 8) | *(a.attention_no.end() - 1);

		std::string attention_msg;
		if (a.attention_msg.has_value()) {
			attention_msg = std::string{a.attention_msg->begin(), a.attention_msg->end()};
		}
		throw std::system_error(attention_code, get_sml_category(), attention_msg);
	}
};

cosem::object sml_to_cosem(const ::sml::file& response)
{
	auto log = spdlog::get("sml");

	std::optional<cosem::object> response_object;

	for (const auto& message : response.messages) {
		auto o = std::visit(message_body_visitor{}, message.body);
		if (o.has_value()) {
			response_object = std::move(o);
		}
	}

	if (!response_object.has_value()) {
		throw std::system_error(sml_errc::invalid_response);
	}

	return response_object.value();
}

// TODO: take obis enum instead of array... or struct with each value group?
cosem::object read_object(const device_id& meter_id, const std::array<std::uint8_t, 6>& obis_number,
                          cosem::id cosem_id, transceive_func transceive)
{
	const octet_string transaction_id{0x00}; // TODO
	const octet_string req_file_id{0x00};    // TODO
	const unsigned8 group_no{0};
	const unsigned8 abort_on_error{0};
	const octet_string server_id(meter_id.begin(), meter_id.end());
	const octet_string client_id{0x00}; // TODO read from config

	file request{{
	    message{transaction_id, group_no, abort_on_error,
	            public_open_request{{}, client_id, req_file_id, server_id, {}, {}, {}}},
	    message{transaction_id, group_no, abort_on_error,
	            get_proc_parameter_request{
	                server_id, {}, {}, parameter_tree_path(obis_number, cosem_id), {}}},
	    message{transaction_id, group_no, abort_on_error, public_close_request{}},
	}};

	std::optional<vec<u8>> buffer = std::nullopt;
	while (!buffer.has_value()) {
		buffer = transceive(meter_id, serialize_file(request));
	}
	auto response = parse_file<true>(buffer->begin(), buffer->end());

	if (response.messages.size() != 3) {
		for (const auto& message : response.messages) {
			std::visit(overloaded{
			               [](const attention_response& a) {
				               int attention_code =
				                   (*(a.attention_no.end() - 2) << 8) | *(a.attention_no.end() - 1);
				               throw std::system_error(attention_code, get_sml_category());
			               },
			               [](const auto&) { throw std::system_error(sml_errc::invalid_response); },
			           },
			           message.body);
		}
	}

	return sml_to_cosem(response);
}

// TODO: everything
void write_object(const device_id& meter_id, transceive_func transceive/*,
                  const std::array<std::uint8_t, 6>& obis_number, cosem::object&& cosem_object*/)
{
	const octet_string transaction_id{0x00}; // TODO
	const unsigned8 group_no{0};
	const unsigned8 abort_on_error{0};
	const octet_string server_id(meter_id.begin(), meter_id.end());
	const octet_string client_id{0x00}; // TODO read from config

	auto request = file{{
	    message{transaction_id, group_no, abort_on_error,
	            public_open_request{{}, client_id, {0}, server_id, {}, {}, {}}},
	    message{transaction_id, group_no, abort_on_error,
	            set_proc_parameter_request{
	                server_id,
	                {},
	                {},
	                tree_path{{{0x01, 0x00, 0x5e, 0x31, 0x00, 0x07, 0, 1, 0},
	                           {0, 2}}}, // TODO: write functions that do this correctly
	                tree{{0x01, 0x00, 0x5e, 0x31, 0x00, 0x07, 0, 1, 0},
	                     {},
	                     sequence<tree>{tree{{0, 2}, value{true}, {}}}}}},
	    message{transaction_id, group_no, abort_on_error, public_close_request{}},
	}};

	// TODO: CLEANUP
	// TODO: interpret attention codes for crypto reset correctly

	std::optional<vec<u8>> buffer = std::nullopt;
	while (!buffer.has_value()) {
		buffer = transceive(meter_id, serialize_file(request));
	}
	auto response = parse_file<true>(buffer->begin(), buffer->end());
	sml_to_cosem(response);

	return;
}

} // namespace smgw::sml
