#include "config.h"
#include "http_server.h"
#include <fstream>
#include <smgw/lmn_interface.h>
#include <smgw/log.h>
#include <yaml-cpp/yaml.h>

int main(int argc, char** argv)
{
	try {
		smgw::configuration cfg = read_configuration(argc, argv);
		smgw::lmn_interface lmn(read_configuration(argc, argv), read_meter_profiles(),
		                        write_meter_profiles);

		http_server server{"0.0.0.0", cfg.http_port, request_handler{lmn}};
		server.serve();
	}
	catch (const std::system_error& e) {
		fmt::print("Error: {}\nAborting.\n", e.what());
	}
}
