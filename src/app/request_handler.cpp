#include "request_handler.h"
#include <nlohmann/json.hpp>
#include <regex>

namespace http = boost::beast::http;
using namespace smgw;
using namespace nlohmann;

namespace nlohmann
{
template <typename... Args>
struct adl_serializer<std::variant<Args...>>
{
	static void to_json(json& j, std::variant<Args...> const& v)
	{
		std::visit([&](auto&& value) { j = std::forward<decltype(value)>(value); }, v);
	}
};
} // namespace nlohmann

// integrating this into nlohmann::json does not work because of cyclic dependencies?
class cosem_serializer
{
public:
	auto operator()(cosem::data const& reading)
	{
		return json{{"logical_name", reading.logical_name}, {"value", reading.value}};
	}

	auto operator()(cosem::registr const& reading)
	{
		return json{{"logical_name", reading.logical_name},
		            {"value", reading.value},
		            {"scaler_unit",
		             {{"scaler", reading.scaler_unit.scaler},
		              {"unit", cosem::to_string(reading.scaler_unit.unit)}}}};
	}

	auto operator()(cosem::signed_extended_register const& reading)
	{
		return json{{"logical_name", reading.logical_name},
		            {"value", reading.value},
		            {"scaler_unit",
		             {{"scaler", reading.scaler_unit.scaler},
		              {"unit", cosem::to_string(reading.scaler_unit.unit)}}},
		            {"status", reading.status},
		            {"capture_time", reading.capture_time},
		            {"signature", reading.signature}};
	}

	auto operator()(cosem::advanced_extended_register const& reading)
	{
		return json{{"logical_name", reading.logical_name},
		            {"value", reading.value},
		            {"scaler_unit",
		             {{"scaler", reading.scaler_unit.scaler},
		              {"unit", cosem::to_string(reading.scaler_unit.unit)}}},
		            {"status", reading.status},
		            {"capture_time", reading.capture_time}};
	}
};

std::string iso_date_time_str(std::chrono::system_clock::time_point now)
{
	using namespace std::chrono;
	auto const now_time_t = system_clock::to_time_t(now);
	auto const tm         = std::gmtime(&now_time_t);
	auto const ms         = duration_cast<milliseconds>(now.time_since_epoch() -
                                                floor<seconds>(now.time_since_epoch()));

	return fmt::format("{:04d}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}.{:03d}Z", tm->tm_year + 1900,
	                   tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
	                   ms.count());
}

auto request_handler::operator()(http::request<http::string_body>&& req) const
    -> http::response<http::string_body>
{
	auto log = spdlog::get("smgw");
	auto url = req.target().to_string();
	log->info("Requested URL: {}", url);

	auto res = http::response<http::string_body>{};
	res.keep_alive(req.keep_alive());
	res.set(boost::beast::http::field::content_type, "application/json");

	// regex for request url
	// format: /cosem/meter/<meterid>/object/<cosemid>-<obiscode>
	// example: /cosem/meter/1EMH0005813766/object/32770-0100010800FF
	static const auto url_regex =
	    std::regex{"^/cosem/meter/([1-9][a-zA-Z]{3}[a-fA-F0-9]{2}[0-9]{8})"
	               "/object/([0-9]{1,5})-([a-fA-F0-9]{12})/?",
	               std::regex_constants::optimize};

	// regex for sym certificate exchange
	// format: /crypto_init/meter/<meterid>
	// example: /crypto_init/meter/1EMH0005813766
	static const auto sym_regex =
	    std::regex{"^/crypto_init/meter/([1-9][a-zA-Z]{3}[a-fA-F0-9]{2}[0-9]{8})/?",
	               std::regex_constants::optimize};

	static const auto crypto_reset_regex =
	    std::regex{"^/crypto_reset/meter/([1-9][a-zA-Z]{3}[a-fA-F0-9]{2}[0-9]{8})/?",
	               std::regex_constants::optimize};

	std::smatch match{};
	if (std::regex_match(url, match, url_regex)) {
		auto meter_id  = device_id_str_to_hex(match[1].str());
		auto cosem_id  = static_cast<cosem::id>(std::stoul(match[2].str()));
		auto obis_code = static_cast<obis::code>(stoull(match[3].str(), nullptr, 16));
		log->debug("Requested URL matched. Meter: {:02X}, COSEM: {}, OBIS: {:012X}",
		           fmt::join(meter_id, ""), enum_val(cosem_id), enum_val(obis_code));

		try {
			auto response_object = lmn.cosem_request(meter_id, obis_code, cosem_id);
			auto now             = std::chrono::system_clock::now();

			auto response_json         = std::visit(cosem_serializer{}, response_object);
			response_json["timestamp"] = iso_date_time_str(now);

			res.body() = response_json.dump();
		}
		catch (std::system_error const& e) {
			log->error("Error while processing request: {}: {}", e.code(), e.what());
			res.result(http::status::internal_server_error);
			res.body() = json{{"error", e.what()}}.dump();
		}
	}
	else if (std::regex_match(url, match, sym_regex)) {
		auto meter_id = device_id_str_to_hex(match[1].str());
		log->debug("Requested URL matched. newLMNCertificate for meter {:02X}",
		           fmt::join(meter_id, ""));
		try {
			lmn.crypto_init(meter_id);
			res.body() = json{{"status", "key exchange successful"}}.dump();
		}
		catch (std::runtime_error const& e) {
			log->error("Error while processing request: {}", e.what());
			res.result(http::status::internal_server_error);
			res.body() = json{{"error", e.what()}}.dump();
		}
	}
	else if (std::regex_match(url, match, crypto_reset_regex)) {
		auto meter_id = device_id_str_to_hex(match[1].str());
		log->debug("Requested URL matched. cryptoReset for meter {:02X}", fmt::join(meter_id, ""));
		try {
			lmn.crypto_reset(meter_id);
			res.body() = json{{"todo", "todo"}}.dump();
		}
		catch (std::runtime_error const& e) {
			log->error("Error while processing request: {}", e.what());
			res.result(http::status::internal_server_error);
			res.body() = json{{"error", e.what()}}.dump();
		}
	}
	else if (url == "/connected_meters") {
		auto connected_meters = lmn.connected_meters();
		json response{};
		for (auto const& meter_id : connected_meters) {
			response["connected_meters"].push_back(device_id_hex_to_str(meter_id));
		}
		res.body() = response.dump();
	}
	else {
		log->debug("Requested URL did not match");
		res.result(http::status::not_found);
		res.body() = json{{"error", "not found"}}.dump();
	}

	return res;
}
