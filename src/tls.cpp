#include "smgw/tls.h"
#include "smgw/error.h"
#include "smgw/log.h"
#include "smgw/sym.h"
#include "utility/openssl_helper.h"
#include <cmath>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

namespace smgw::tls
{

using namespace utility;

manager::manager(const meter_profile_store& meter_profiles_)
    : tls_context{SSL_CTX_new(TLS_client_method())}, meter_profiles{meter_profiles_}
{
	openssl_return_code ret;

	SSL_load_error_strings();
	ret = SSL_CTX_set_min_proto_version(tls_context.get(), TLS1_2_VERSION);
	ret = SSL_CTX_set_max_proto_version(tls_context.get(), TLS1_2_VERSION);
	ret = SSL_CTX_set_cipher_list(tls_context.get(), "ECDHE-ECDSA-AES128-SHA256:"
	                                                 "ECDHE-ECDSA-AES256-SHA384:"
	                                                 "ECDHE-ECDSA-AES128-GCM-SHA256:"
	                                                 "ECDHE-ECDSA-AES256-GCM-SHA384");
	// note: prime256v1 == secp256r1
	std::vector<int> curves{NID_brainpoolP256r1, NID_brainpoolP384r1, NID_brainpoolP512r1,
	                        NID_X9_62_prime256v1, NID_secp384r1};
	ret = SSL_CTX_set1_curves(tls_context.get(), curves.data(), static_cast<int>(curves.size()));

	SSL_CTX_set_options(tls_context.get(), SSL_OP_NO_RENEGOTIATION);
	SSL_CTX_set_verify(tls_context.get(), SSL_VERIFY_NONE, nullptr);

	auto key_pair = sym::get_gateway_cert_key();

	ret = SSL_CTX_use_certificate_ASN1(tls_context.get(), key_pair.certificate.size(),
	                                   key_pair.certificate.data());
	ret = SSL_CTX_use_PrivateKey_ASN1(EVP_PKEY_EC, tls_context.get(), key_pair.private_key.data(),
	                                  key_pair.private_key.size());
}

session& manager::get_session(const device_id& meter_id)
{
	// TODO look for certs and keys in meter profiles, error if not present

	if (contains(active_sessions, meter_id)) {
		return active_sessions[meter_id];
	}

	auto profile = meter_profiles.get(meter_id);
	if (!profile || !profile->max_fragment_length) {
		throw std::system_error(tls_errc::missing_certs_and_keys);
	}

	auto tls_session = ssl_ptr(SSL_new(tls_context.get()));
	if (!tls_session) {
		handle_openssl_error();
	}

	{
		// BIOs are automatically freed by SSL_free

		// where openssl reads from and the application writes to
		auto rbio = BIO_new(BIO_s_mem());
		if (!rbio) {
			handle_openssl_error();
		}

		// where openssl writes to and the application reads from
		auto wbio = BIO_new(BIO_s_mem());
		if (!wbio) {
			handle_openssl_error();
		}

		// when BIO is empty, do not return EOF
		BIO_set_mem_eof_return(rbio, -1);
		BIO_set_mem_eof_return(wbio, -1);

		SSL_set_bio(tls_session.get(), rbio, wbio);
	}

	u8 max_frag_val = [&profile]() {
		switch (profile->max_fragment_length.value()) {
		case 512: return TLSEXT_max_fragment_length_512;
		case 1024: return TLSEXT_max_fragment_length_1024;
		case 2048:
		default: return TLSEXT_max_fragment_length_2048;
		}
	}();
	SSL_set_tlsext_max_fragment_length(tls_session.get(), max_frag_val);

	SSL_set_connect_state(tls_session.get());

	active_sessions.emplace(meter_id, session{std::move(tls_session)});
	return active_sessions[meter_id];
}

void manager::delete_session(const device_id& meter_id)
{
	active_sessions.erase(meter_id);
}

std::optional<vec<u8>> transceiver::operator()(const device_id& meter_id,
                                               std::optional<vec<u8>>&& payload)
{

	auto log = spdlog::get("tls");

	if (!payload.has_value()) {
		log->debug("no payload supplied: doing nothing.");
		return std::nullopt;
	}

	auto tls_session = active_session.tls_session.get();
	auto wbio        = SSL_get_wbio(tls_session);
	auto rbio        = SSL_get_rbio(tls_session);

	vec<u8> response_plaintext;
	int num_bytes          = 0;
	bool response_received = false;

	while (!response_received) {
		// write paintext data to ssl session
		ERR_clear_error();
		num_bytes = SSL_write(tls_session, payload->data(), static_cast<int>(payload->size()));

		if (num_bytes > 0) {
			// handshake is complete, request has been written
			log->debug("TLS handshake finished. Request payload plaintext: {} bytes\n{}",
			           payload->size(), log_helper{*payload});
			num_bytes = static_cast<int>(BIO_ctrl_pending(rbio));
			response_plaintext.resize(num_bytes);
			ERR_clear_error();
			num_bytes = SSL_read(tls_session, response_plaintext.data(), num_bytes);
			if (num_bytes > 0) {
				response_received = true;
				continue;
			}
		}
		else {
			log->debug("Performing TLS handshake");
		}

		if (SSL_get_error(tls_session, num_bytes) != SSL_ERROR_WANT_READ) {
			// will SSL_ERROR_WANT_WRITE ever happen?
			handle_openssl_error();
		}

		// read ciphertext from memory BIO
		num_bytes = static_cast<int>(BIO_ctrl_pending(wbio));
		std::optional<vec<u8>> request_ciphertext;
		if (num_bytes > 0) {
			request_ciphertext = vec<u8>(num_bytes);
			BIO_read(wbio, request_ciphertext->data(), num_bytes);
		}

		// transmit ciphertext, receive response
		auto response_ciphertext = transceive(meter_id, std::move(request_ciphertext));
		if (!response_ciphertext.has_value()) {
			continue; // peer needs more data
		}

		// write received ciphertext to memory BIO
		BIO_write(rbio, response_ciphertext->data(), static_cast<int>(response_ciphertext->size()));

		// try to read plaintext response from ssl session
		num_bytes = static_cast<int>(BIO_ctrl_pending(rbio));
		response_plaintext.resize(response_ciphertext.value().size());
		ERR_clear_error();
		num_bytes = SSL_read(tls_session, response_plaintext.data(), num_bytes);
		if (num_bytes > 0) {
			response_received = true;
		}
	}

	response_plaintext.resize(num_bytes);
	log->debug("Response payload plaintext: {} bytes\n{}", response_plaintext.size(),
	           log_helper{response_plaintext});
	return response_plaintext;
}

} // namespace smgw::tls
