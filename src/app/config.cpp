#include <smgw/config.h>
#include <smgw/meter_profile.h>
#include <smgw/utility/utility.h>
using namespace smgw;

#include <fstream>

#include <args.hxx>
#include <yaml-cpp/yaml.h>
#include <iostream>

smgw::configuration read_configuration(int argc, char** argv)
{
	configuration cfg;

	// set up command line parser
	args::ArgumentParser parser("LMN SMGw Simulator");
	parser.Prog("lmn_smgw_simulator");
	args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});

	args::ValueFlag<std::string> config_file_path(parser, "path", "Config file path",
	                                              {'c', "config"}, "config.yml");

	const std::unordered_map<std::string, spdlog::level::level_enum> log_level_map{
	    {"trace", spdlog::level::trace}, {"debug", spdlog::level::debug},
	    {"info", spdlog::level::info},   {"warning", spdlog::level::warn},
	    {"error", spdlog::level::err},   {"critical", spdlog::level::critical},
	    {"off", spdlog::level::off}};
	args::MapFlag<std::string, spdlog::level::level_enum> log_level(
	    parser, "level", "Log level {trace | debug | info | warning | error | critical | off}",
	    {'l', "level"}, log_level_map);

	args::ValueFlag<std::string> port_name(parser, "port", "Serial port name", {'p', "port"});
	args::ValueFlag<std::string> port_host(parser, "httpport", "http port ", {'o', "httpport"});

	try {
		parser.ParseCLI(argc, argv);
	}
	catch (const args::Help&) {
        std::cout << parser;
		std::exit(EXIT_SUCCESS);
	}
	catch (const args::ParseError& e) {
		fmt::print("Error while parsing command line arguments: {}", e.what());
		std::exit(EXIT_FAILURE);
	}

	// set up config file parser
	YAML::Node config_file;
	try {
		config_file = YAML::LoadFile(config_file_path.Get());
	}
	catch (const YAML::ParserException& e) {
		fmt::print("Error while parsing config file {}. Using default configuration. {}\n",
		           config_file_path.Get(), e.what());
	}
	catch (const YAML::BadFile& e) {
		fmt::print("Config file {} could not be found. Using default configuration. {}\n",
		           config_file_path.Get(), e.what());
	}

	// read config
	if (log_level) {
		cfg.log_configuration.log_level = log_level.Get();
	}
	else if (config_file["log"]["log_level"]) {
		auto log_level_string = config_file["log"]["log_level"].as<std::string>();
		if (contains(log_level_map, log_level_string)) {
			cfg.log_configuration.log_level = log_level_map.at(log_level_string);
		}
	}

	if (port_host) {
		cfg.http_port = std::stoi(port_host.Get());
	}
	if (port_name) {
		cfg.serial_configuration.port_name = port_name.Get();
	}
	else if (config_file["serial"]["port_name"]) {
		cfg.serial_configuration.port_name = config_file["serial"]["port_name"].as<std::string>();
	}

	return cfg;
}

std::optional<vec<u8>> ascii_to_hex(std::string&& str)
{
	str.erase(std::remove_if(str.begin(), str.end(), [](char c) { return c == ' '; }), str.end());
	std::transform(str.begin(), str.end(), str.begin(), [](char c) { return std::toupper(c); });
	if ((str.size() % 2) != 0) {
		return std::nullopt;
	}

	auto hex2i = [](char c) -> std::uint8_t { return c <= '9' ? c - '0' : c - 'A' + 10; };

	vec<u8> result;
	result.reserve(str.size() / 2);
	for (std::size_t i = 0; i < (str.size() / 2); i++) {
		result.push_back((hex2i(str[2 * i]) << 4) | hex2i(str[2 * i + 1]));
	}

	return result;
}

namespace YAML
{
template <>
struct convert<vec<u8>>
{
	static bool decode(const Node& node, vec<u8>& rhs)
	{
		auto v = ascii_to_hex(node.as<std::string>());
		if (v) {
			rhs = std::move(v.value());
			return true;
		}
		return false;
	}
};

template <>
struct convert<device_id>
{
	static Node encode(const device_id& rhs)
	{
		Node node;
		node = device_id_hex_to_str(rhs);
		return node;
	}

	static bool decode(const Node& node, device_id& rhs)
	{
		rhs = device_id_str_to_hex(node.as<std::string>());
		return true;
	}
};

template <>
struct convert<meter_profile>
{
	static Node encode(const meter_profile& rhs)
	{
		Node node;
		node["sensor_key"] = fmt::format("{:02X}", fmt::join(rhs.sensor_key, ""));
		if (rhs.max_fragment_length) {
			node["max_fragment_length"] = rhs.max_fragment_length.value();
		}
		return node;
	}

	static bool decode(const Node& node, meter_profile& rhs)
	{
		rhs.sensor_key = node["sensor_key"].as<vec<u8>>();

		if (node["max_fragment_length"]) {
			rhs.max_fragment_length = node["max_fragment_length"].as<u16>();
		}

		return true;
	}
};
} // namespace YAML

smgw::meter_profile_map read_meter_profiles()
{
	smgw::meter_profile_map meter_profiles;

	try {
		auto meters    = YAML::LoadFile("meters.yml");
		meter_profiles = meters.as<meter_profile_map>();
	}
	catch (const YAML::ParserException& e) {
		fmt::print("Error while parsing meter profiles: {}\n", e.what());
	}
	catch (const YAML::BadFile& e) {
		fmt::print("File meters.yml could not be found. {}\n", e.what());
	}

	return meter_profiles;
}

void write_meter_profiles(smgw::meter_profile_map const& meter_profiles)
{
	try {
		YAML::Node node;
		node = meter_profiles;

		std::ofstream out_file("meters.yml");
		out_file << node;
	}
	catch (std::runtime_error const& e) {
		fmt::print("Error while writing meter profiles: {}\n", e.what());
	}
}
