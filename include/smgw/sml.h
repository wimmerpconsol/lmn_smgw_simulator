#ifndef SMGW_SML_H
#define SMGW_SML_H

#include "cosem.h"
#include "utility/transceive.h"
#include "utility/utility.h"
#include <smlpp/file.h>

namespace smgw::sml
{

cosem::object read_object(const device_id& meter_id, const std::array<std::uint8_t, 6>& obis_number,
                          cosem::id cosem_id, transceive_func transceive);

void write_object(const device_id& meter_id, transceive_func transceive/*,
                  const std::array<std::uint8_t, 6>& obis_number, cosem::object&& cosem_object*/);

cosem::object sml_to_cosem(const ::sml::file& response);

} // namespace smgw::sml

#endif // SMGW_SML_H
