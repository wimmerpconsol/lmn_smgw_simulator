#ifndef SMGW_APP_READ_CONFIG_H
#define SMGW_APP_READ_CONFIG_H

#include <smgw/config.h>
#include <smgw/meter_profile.h>

smgw::configuration read_configuration(int argc, char** argv);
smgw::meter_profile_map read_meter_profiles();
void write_meter_profiles(smgw::meter_profile_map const& meter_profiles);

#endif // SMGW_APP_READ_CONFIG_H
