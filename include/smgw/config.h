#ifndef SMGW_CONFIG_H
#define SMGW_CONFIG_H

#include "log.h"
#include "serial/config.h"

namespace smgw
{

struct configuration
{
	log::configuration log_configuration;
	serial::configuration serial_configuration;
	uint16_t http_port{8080};
};

} // namespace smgw

#endif // SMGW_CONFIG_H
