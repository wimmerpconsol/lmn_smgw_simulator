#ifndef SMGW_COSEM_H
#define SMGW_COSEM_H

#include <cstdint>
#include <smlpp/unit.h>
#include <variant>
#include <vector>

namespace smgw::cosem
{

template <typename... Ts>
using choice = std::variant<Ts...>;

using boolean      = bool;
using unsigned32   = std::uint32_t;
using octet_string = std::vector<std::uint8_t>;
using integer8     = std::int8_t;
using unsigned16   = std::uint16_t;
using signed64     = std::int64_t;
using unsigned64   = std::uint64_t;
using unit_type    = ::sml::unit;

struct scal_unit_type
{
	integer8 scaler;
	unit_type unit;
};

enum class id : std::uint16_t
{
	data                       = 1,
	registr                    = 3,
	signed_extended_register   = 32768,
	advanced_extended_register = 32770
};

struct data
{
	octet_string logical_name;
	choice<boolean, unsigned32, octet_string, unsigned16> value;
};

// "register" is a keyword and can't be used
struct registr
{
	octet_string logical_name;
	choice<signed64, unsigned64> value;
	scal_unit_type scaler_unit;
};

struct signed_extended_register
{
	octet_string logical_name;
	unsigned64 value;
	scal_unit_type scaler_unit;
	unsigned32 status;
	unsigned32 capture_time;
	octet_string signature;
};

struct advanced_extended_register
{
	octet_string logical_name;
	unsigned64 value;
	scal_unit_type scaler_unit;
	unsigned32 status;
	unsigned32 capture_time;
};

using object = std::variant<data, registr, signed_extended_register, advanced_extended_register>;

inline constexpr std::size_t num_attributes(id cosem_id)
{
	switch (cosem_id) {
	case id::data: return 2;
	case id::registr: return 3;
	case id::signed_extended_register: return 6;
	case id::advanced_extended_register: return 5;
	default: return 0;
	}
}

std::string to_string(unit_type u);

} // namespace smgw::cosem

#endif // SMGW_COSEM_H
