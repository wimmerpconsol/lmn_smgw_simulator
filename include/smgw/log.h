#ifndef SMGW_LOG_H
#define SMGW_LOG_H

#include "hdlc/frame.h"
#include "hdlc/lmn/master.h"
#include "utility/utility.h"

#define SPDLOG_LEVEL_NAMES \
	{ \
		"trace", "debug", "info ", "warn.", "error", "crit.", "off " \
	}
#include <spdlog/fmt/ostr.h>
#include <spdlog/spdlog.h>

namespace smgw
{

namespace log
{

struct configuration
{
	spdlog::level::level_enum log_level{spdlog::level::info};
};

void init(const configuration& cfg = configuration{});

std::shared_ptr<spdlog::logger> make_logger(const std::string& name);

} // namespace log

// helper to disambiguate logging of vectors
template <typename T>
struct log_helper
{
	const T& data;
};

// deduction guide for template class argument deduction
template <typename T>
log_helper(T t)->log_helper<T>;

std::ostream& operator<<(std::ostream& os, log_helper<vec<u8>> raw_bytes);

namespace hdlc
{
std::ostream& operator<<(std::ostream& os, const frame& hdlc_frame);
namespace lmn
{
std::ostream& operator<<(std::ostream& os, log_helper<slave_map> known_slaves);
}
} // namespace hdlc

} // namespace smgw

#endif // SMGW_LOG_H
